/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var workloads =
[
  {
    "workload_parameters" : {
        "image" : "severalnines/sysbench",
        "cmd" : "/bin/sh",
        "args" : "-c\nfor i in $N_THREADS; do echo \"###$TEST###$i###\"; sysbench $TEST run $OPTIONS --time=0 --threads=$i; echo \"###END###\"; done",
        "N_THREADS" : "1 2 4 8 16 32",
        "TEST" : "cpu",
        "OPTIONS" : "--cpu-max-prime=20000 --events=10000"
    },
    "categories" : [ 
        "cpu"
    ],
    "grants" : {
        "executor" : "org:public",
        "owner" : "usr:august"
    },
    "parser" : "benchsuite.stdlib.benchmark.parsers.SysbenchResultParser",
    "class" : "benchsuite.stdlib.benchmark.docker_benchmark.DockerBenchmark",
    "description" : "first docker workload",
    "id" : "90f8ea37-4881-4edf-89b3-0bb97bf9993e",
    "tool_name" : "SysBench",
    "workload_name" : "docker_cpu_10000"
  },
  {
    "workload_name": "varmail-short",
    "tool_name": "FileBench",
    "id": "cee326cb-806c-4624-b65c-e0f365030590",
    "description": "Emulates I/O activity of a simple mail server that stores each e-mail in a separate file.",
    "categories": [
      "Mail Server",
      "Filesystem"
    ],
    "parent_workload_id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "workload_parameters": {
      "workload_file": "varmail.f",
      "run_param": "30",
      "custom_params": " "
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "videoserver",
    "tool_name": "FileBench",
    "id": "31a6365f-1878-431c-b3d1-bde5ffecfb7f",
    "description": "This workloads emulates a video server. It has two filesets: one contains videos that are actively served, and the second one has videos that are available but currently inactive. ",
    "categories": [
      "Video Server",
      "Filesystem"
    ],
    "parent_workload_id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "workload_parameters": {
      "run_param": "300",
      "custom_params": "filesize=10m",
      "workload_file": "videoserver.f"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "varmail",
    "tool_name": "FileBench",
    "id": "d259ad9d-97aa-49e7-9f61-97a89a7cd0f3",
    "description": " Emulates I/O activity of a simple mail server that stores each e-mail in a separate file. ",
    "categories": [
      "Mail Server",
      "Filesystem"
    ],
    "parent_workload_id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "workload_parameters": {
      "run_param": "300",
      "custom_params": " ",
      "workload_file": "varmail.f"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "WFB Django Query",
    "tool_name": "Web Framework Benchmarking",
    "id": "67f9e180-b644-4d7a-8470-df34c3e7cf49",
    "categories": [],
    "install": "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -\nsudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install docker-ce unzip\n# download FrameworkBenchmarks from master\ntest ! -f /tmp/frameworkBenchmarks.zip && wget https://codeload.github.com/TechEmpower/FrameworkBenchmarks/zip/master -O /tmp/frameworkBenchmarks.zip || echo \"File already exist. Do not download\"\nunzip /tmp/frameworkBenchmarks.zip\nmv FrameworkBenchmarks-master tool\n# remove the interactive option from the docker invocation, otherwise it will fail\n# when launched from the Benchmarking Suite\nsed -i 's/exec\\ docker\\(.*\\)-it/exec\\ docker\\1/' tool/tfb",
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install docker.io unzip\n# download FrameworkBenchmarks from master\ntest ! -f /tmp/frameworkBenchmarks.zip && wget https://codeload.github.com/TechEmpower/FrameworkBenchmarks/zip/master -O /tmp/frameworkBenchmarks.zip || echo \"File already exist. Do not download\"\nunzip /tmp/frameworkBenchmarks.zip\nmv FrameworkBenchmarks-master tool\n# remove the interactive option from the docker invocation, otherwise it will fail\n# when launched from the Benchmarking Suite\nsed -i 's/exec\\ docker\\(.*\\)-it/exec\\ docker\\1/' tool/tfb",
    "execute": "# if there is < 2GB available, try to prune Docker objects to get more space\ntest `df -k --output=avail / |tail -1` -lt 2000000 && sudo docker system prune -af\ncd tool\nsudo ./tfb --test %(framework)s --type %(test)s | tee output && echo -e \"@@@ results.json content @@@\\n\" && cat `find results -name results.json` && echo -e \"\\n@@@@@@\"\n# kill all containers (if test fails, some containers might remain running)\ntest `sudo docker ps -q | wc -l` -ne \"0\" && sudo docker kill $(sudo docker ps -q)\n# since the tfb command does not exist with a status !=0 in case of failures, we store the output and search for \"NO RESULTS\" string\n! grep \"NO RESULTS\" output",
    "workload_parameters": {
      "test": "query",
      "framework": "django"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:gabriele"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "webproxy",
    "tool_name": "FileBench",
    "id": "3e3bebee-ad07-437f-af67-056d7a6a72a7",
    "description": "Emulates I/O activity of a simple web proxy server. A mix of create-write-close, open-read-close, and delete operations of multiple files in a directory tree and a file append to simulate proxy log.",
    "categories": [
      "Web Proxy",
      "Filesystem"
    ],
    "parent_workload_id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "workload_parameters": {
      "custom_params": "nthreads=75",
      "run_param": "300",
      "workload_file": "webproxy.f"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "webserver",
    "tool_name": "FileBench",
    "id": "2e0f10b0-b9a1-4b7c-a7bc-26b59b9cca6f",
    "description": "Emulates simple web-server I/O activity. Produces a sequence of open-read-close on multiple files in a directory tree plus a log file append.",
    "categories": [
      "Web Server",
      "Filesystem"
    ],
    "parent_workload_id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "workload_parameters": {
      "workload_file": "webserver.f",
      "run_param": "300",
      "custom_params": "nthreads=75"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "fileserver",
    "tool_name": "FileBench",
    "id": "6e6e5177-549b-4261-b395-e5416a1cfe5b",
    "description": "Emulates simple file-server I/O activity. This workload performs a sequence of creates, deletes, appends, reads, writes and attribute operations on a directory tree.",
    "categories": [
      "File Server",
      "Filesystem"
    ],
    "parent_workload_id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "workload_parameters": {
      "custom_params": " ",
      "run_param": "220",
      "workload_file": "fileserver.f"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "cpu_10000",
    "tool_name": "SysBench",
    "id": "2e358596-b13b-4eb1-8865-dc1ef9c7a44b",
    "description": "Execute 1000 cycles of prime numbers between 0 and 20000 verification (done using divisions).",
    "categories": [
      "CPU",
      "Prime Numbers"
    ],
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install curl\ncurl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install sysbench\nsysbench %(test)s prepare %(options)s || echo \"not implemented\"",
    "install_ubuntu": "%(install_debian)s",
    "execute": "for i in %(n_threads)s; do echo \"###%(test)s###$i###\"; sysbench %(test)s run %(options)s --time=0 --threads=$i; echo \"###END###\"; done",
    "cleanup": "sysbench %(test)s cleanup %(options)s || echo \"not implemented\"",
    "workload_parameters": {
      "test": "cpu",
      "n_threads": "1 2 4 8 16 32",
      "options": "--cpu-max-prime=20000 --events=10000"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.SysbenchResultParser"
  },
  {
    "workload_name": "YCSB Workload A",
    "tool_name": "YCSB MySQL",
    "id": "2bcc1caf-97d3-466a-b28c-fba12e0d52d0",
    "categories": [
      "database",
      "mysql"
    ],
    "description": "The application has read/update operations ratio of 50/50. Example: session store recording recent actions.",
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install mariadb-server openjdk-11-jre-headless python libmariadb-java\nsudo cp /usr/share/java/mariadb-java-client.jar /tmp/driver.jar\necho \"\ncreate database benchmark;\nCREATE user 'testuser'@localhost IDENTIFIED BY 'password';\nGRANT ALL ON benchmark.* TO 'testuser' IDENTIFIED BY 'password';\n\" | sudo mysql\n%(install_all)s",
    "install_all": "test ! -f /tmp/ycsb-jdbc.tar.gz && wget https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-jdbc-binding-0.17.0.tar.gz -O /tmp/ycsb-jdbc.tar.gz || echo \"File already exist. Do not download\"\ntar xzvf /tmp/ycsb-jdbc.tar.gz\nmv ycsb-jdbc-binding-0.17.0 ycsb\necho \"\nuse benchmark;\ndrop table if exists usertable;\nCREATE TABLE usertable (\n\tYCSB_KEY VARCHAR(255) PRIMARY KEY,\n\tFIELD0 TEXT, FIELD1 TEXT,\n\tFIELD2 TEXT, FIELD3 TEXT,\n\tFIELD4 TEXT, FIELD5 TEXT,\n\tFIELD6 TEXT, FIELD7 TEXT,\n\tFIELD8 TEXT, FIELD9 TEXT\n);\n\" | mysql -utestuser -ppassword\ncd ycsb\necho \"%(workload)s\" > workload.txt\nbin/ycsb load jdbc -P workload.txt -p db.url=jdbc:mysql://127.0.0.1:3306/benchmark -p db.user=testuser -p db.passwd=password -p db.batchsize=10000 -p jdbc.batchupdateapi=true -p jdbc.autocommit=false -p db.driver=org.mariadb.jdbc.Driver -cp /tmp/driver.jar | tee output\n# since the ycsb commands do not exit with an exit status !=0 if the load or run contain errors, we search for the FAILED word in the output\n! grep FAILED output",
    "execute": "cd ycsb\nbin/ycsb run jdbc -P workload.txt -p db.url=jdbc:mysql://127.0.0.1:3306/benchmark -p db.user=testuser -p db.passwd=password -p db.driver=org.mariadb.jdbc.Driver -cp /tmp/driver.jar | tee output\n# since the ycsb commands do not exit with an exit status !=0 if the load or run contain errors, we search for the FAILED word in the output\n! grep FAILED output",
    "workload_parameters": {
      "workload": "recordcount=%(default_ops_count)s\noperationcount=%(default_ops_count)s\nworkload=site.ycsb.workloads.CoreWorkload\nreadallfields=true\nreadproportion=0.5\nupdateproportion=0.5\nscanproportion=0\ninsertproportion=0\nrequestdistribution=zipfian",
      "default_ops_count": "100000"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:gabriele"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
  },
  {
    "workload_name": "tomcat",
    "tool_name": "DaCapo",
    "id": "510b1c85-fc51-4b96-beb9-6736ccb3ef0a",
    "description": "Runs a set of queries against a Tomcat server retrieving and verifying the resulting webpages",
    "categories": [
      "java",
      "Web Server"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "tomcat"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "luindex",
    "tool_name": "DaCapo",
    "id": "faa4cd88-279f-45c2-a043-543a646602c3",
    "description": "Uses Lucene to index a set of documents.",
    "categories": [
      "document indexing",
      "Lucene"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "luindex"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "lusearch",
    "tool_name": "DaCapo",
    "id": "d9119bcf-bb50-4d12-a43b-ed10ad7cd8d0",
    "description": "Uses Lucene to do a text search of keywords over a corpus of data",
    "categories": [
      "Lucene",
      "document search"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "lusearch"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "avrora",
    "tool_name": "DaCapo",
    "id": "076ff3d1-b525-450c-8141-405ccd62ccb2",
    "description": "Simulates a network of sensors based on the AVR microcontrollers.",
    "categories": [
      "sensor network",
      "IoT"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "avrora"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "eclipse",
    "tool_name": "DaCapo",
    "id": "b825f407-15e5-4360-85f7-e71f52b5c289",
    "description": "Simulates typical operations in an Eclipse Workspace (file open/save, search, indexing)",
    "categories": [
      "eclipse",
      "software development"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "eclipse"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "fop",
    "tool_name": "DaCapo",
    "id": "ffa206e2-c604-4ab1-9282-9017ec8a2452",
    "description": "Uses Apache FOP to perform document formatting",
    "categories": [
      "Apache FOP",
      "document transformation",
      "document formatting"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "fop"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "h2",
    "tool_name": "DaCapo",
    "id": "b512ee91-7749-428c-b834-4bc57b3c1d9d",
    "description": "Simulate a Java relational database (H2)",
    "categories": [
      "Relational Database",
      "database",
      "h2"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "h2"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "jython",
    "tool_name": "DaCapo",
    "id": "d54da271-2e61-4dfa-9268-cec1d60ccedc",
    "description": "Test execution of Python code through Jython",
    "categories": [
      "python ",
      "jython"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "jython"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "pmd",
    "tool_name": "DaCapo",
    "id": "d85d958f-667e-4515-959e-38a46b362e11",
    "description": "Execute static code analysis with PMD for Java",
    "categories": [
      "pmd",
      "static code analysis"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "pmd"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "sunflow",
    "tool_name": "DaCapo",
    "id": "29da4dc9-2504-46f6-abb0-7027b2c54a69",
    "description": "Simulate a photo realistic rendering of a scene with SunFlow",
    "categories": [
      "Photo Realistic Rendering",
      "rendering"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "sunflow"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "tradebeans",
    "tool_name": "DaCapo",
    "id": "9c8e95a2-7915-404c-b1ad-d60f8bc73d66",
    "description": "A Java EJB application that simulates a Stock trading website",
    "categories": [
      "EJB",
      "Stock Trading Online"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "tradebeans"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "tradesoap",
    "tool_name": "DaCapo",
    "id": "20b0d93e-bcb8-4258-ae7d-4497a08dda72",
    "description": "A SOAP-based Java application that simulates a Stock trading website",
    "categories": [
      "SOAP",
      "Stock Trading Online"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "tradesoap"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "xalan",
    "tool_name": "DaCapo",
    "id": "1edd01eb-24b1-4da2-8afe-4baece465d7c",
    "description": "Perform XSLT transformation of XML documents",
    "categories": [
      "XSLT",
      "Document Transformation",
      "XML"
    ],
    "parent_workload_id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "workload_parameters": {
      "workload_id": "xalan"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "django-plaintext",
    "tool_name": "Web Framework Benchmarking",
    "id": "c60564b0-f07c-4d06-9584-09b6a9d0e8d3",
    "description": "An exercise of the request-routing fundamentals only, designed to demonstrate the capacity of high-performance platforms in particular. Requests will be sent using HTTP pipelining.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "django",
      "test": "plaintext"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "django-json",
    "tool_name": "Web Framework Benchmarking",
    "id": "828082dd-dba8-4d5d-b332-e480ddee56c4",
    "description": "Exercises the framework fundamentals including keep-alive support, request routing, request header parsing, object instantiation, JSON serialization, header generation, and request count throughput.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "json",
      "framework": "django"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "django-fortune",
    "tool_name": "Web Framework Benchmarking",
    "id": "acc4d983-4ba9-427d-a45f-c938158b9c3a",
    "description": "Exercises the ORM, database connectivity, dynamic-size collections, sorting, server-side templates, XSS countermeasures, and character encoding.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "django",
      "test": "fortune"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "django-db",
    "tool_name": "Web Framework Benchmarking",
    "id": "7367d0c7-d0d5-407a-af59-4c4bc8307ab4",
    "description": "A variation of \"query\" workload and also uses the World table. Multiple rows are fetched to more dramatically punish the database driver and connection pool.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "django",
      "test": "db"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "django-update",
    "tool_name": "Web Framework Benchmarking",
    "id": "8af3abdd-04c9-43e6-bc88-be76462453cd",
    "description": "A variation of the \"query\" workload that exercises the ORM's persistence of objects and the database driver's performance at running UPDATE statements or similar. ",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "update",
      "framework": "django"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "spring-query",
    "tool_name": "Web Framework Benchmarking",
    "id": "b658f10b-e4b5-421e-92f5-4575d67b2bba",
    "description": "Exercises the Spring framework's object-relational mapper (ORM), random number generator, database driver, and database connection pool.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "query",
      "framework": "spring"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "spring-plaintext",
    "tool_name": "Web Framework Benchmarking",
    "id": "eb398c13-61f7-48d9-bd33-163637430cec",
    "description": "An exercise of the request-routing fundamentals only, designed to demonstrate the capacity of high-performance platforms in particular. Requests will be sent using HTTP pipelining.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "spring",
      "test": "plaintext"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "spring-json",
    "tool_name": "Web Framework Benchmarking",
    "id": "8fac2551-46d6-4c4a-be15-7eca40927364",
    "description": "Exercises the Spring fundamentals including keep-alive support, request routing, request header parsing, object instantiation, JSON serialization, header generation, and request count throughput.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "json",
      "framework": "spring"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "spring-fortune",
    "tool_name": "Web Framework Benchmarking",
    "id": "86b18f71-d5ae-4148-a552-ef5c66292845",
    "description": "Exercises the ORM, database connectivity, dynamic-size collections, sorting, server-side templates, XSS countermeasures, and character encoding.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "spring",
      "test": "fortune"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "spring-db",
    "tool_name": "Web Framework Benchmarking",
    "id": "2201b834-f33c-4303-bd63-5150d70d81cc",
    "description": "A variation of \"query\" workload and also uses the World table. Multiple rows are fetched to more dramatically punish the database driver and connection pool.",
    "categories": [
      "Network Intensive"
    ],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "db",
      "framework": "spring"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "spring-update",
    "tool_name": "Web Framework Benchmarking",
    "id": "646e3ed8-d153-40bf-80b1-d1faf382ba30",
    "description": "A variation of the \"query\" workload that exercises the ORM's persistence of objects and the database driver's performance at running UPDATE statements or similar. ",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "spring",
      "test": "update"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "cakephp-query",
    "tool_name": "Web Framework Benchmarking",
    "id": "b4c95a55-d1ca-42f4-8952-0257b569ffbd",
    "description": "Exercises the CakePHP framework's object-relational mapper (ORM), random number generator, database driver, and database connection pool.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "query",
      "framework": "cakephp"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "cakephp-plaintext",
    "tool_name": "Web Framework Benchmarking",
    "id": "9ddff82b-218e-410d-a6bb-329ae649d34e",
    "description": "An exercise of the request-routing fundamentals only, designed to demonstrate the capacity of high-performance platforms in particular. Requests will be sent using HTTP pipelining.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "cakephp",
      "test": "plaintext"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "cakephp-json",
    "tool_name": "Web Framework Benchmarking",
    "id": "303368d1-6ead-4bdd-901a-a264ca908fe0",
    "description": "Exercises the CakePHP framework fundamentals including keep-alive support, request routing, request header parsing, JSON serialization, header generation, and request count throughput.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "cakephp",
      "test": "json"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "cakephp-fortune",
    "tool_name": "Web Framework Benchmarking",
    "id": "b6b362b0-6e86-4e10-9e16-318f65a93981",
    "description": "Exercises the ORM, database connectivity, dynamic-size collections, sorting, server-side templates, XSS countermeasures, and character encoding.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "cakephp",
      "test": "fortune"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "cakephp-db",
    "tool_name": "Web Framework Benchmarking",
    "id": "212a2c2a-9a2e-4a63-ac3f-e8dabcf62fca",
    "description": "A variation of \"query\" workload and also uses the World table. Multiple rows are fetched to more dramatically punish the database driver and connection pool.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "db",
      "framework": "cakephp"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "cakephp-update",
    "tool_name": "Web Framework Benchmarking",
    "id": "d59cc9a1-f41a-4adb-ad82-992a42ee4a93",
    "description": "A variation of the \"query\" workload that exercises the ORM's persistence of objects and the database driver's performance at running UPDATE statements or similar. ",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "cakephp",
      "test": "update"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "nodejs-query",
    "tool_name": "Web Framework Benchmarking",
    "id": "06e955e1-a8cf-4725-b4a8-da977ccf4cd9",
    "description": "Exercises the Node.js framework's object-relational mapper (ORM), random number generator, database driver, and database connection pool.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "nodejs",
      "test": "query"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "nodejs-plaintext",
    "tool_name": "Web Framework Benchmarking",
    "id": "c5cdddea-dbcc-48a0-a9fe-105ea92a94b1",
    "description": "An exercise of the request-routing fundamentals only, designed to demonstrate the capacity of high-performance platforms in particular. Requests will be sent using HTTP pipelining.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "nodejs",
      "test": "plaintext"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "nodejs-json",
    "tool_name": "Web Framework Benchmarking",
    "id": "bf78d1b1-a382-4823-95fc-e9dd6ad0850b",
    "description": "Exercises the NodeJS fundamentals including keep-alive support, request routing, request header parsing, object instantiation, JSON serialization, header generation, and request count throughput.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "json",
      "framework": "nodejs"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "nodejs-fortune",
    "tool_name": "Web Framework Benchmarking",
    "id": "da4fc5c6-b493-41db-9f45-37207b0d3c4c",
    "description": "Exercises the ORM, database connectivity, dynamic-size collections, sorting, server-side templates, XSS countermeasures, and character encoding.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "nodejs",
      "test": "fortune"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "nodejs-db",
    "tool_name": "Web Framework Benchmarking",
    "id": "e3d8aed9-c672-4411-95c7-fc7fa80da512",
    "description": "A variation of \"query\" workload and also uses the World table. Multiple rows are fetched to more dramatically punish the database driver and connection pool.",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "db",
      "framework": "nodejs"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "nodejs-update",
    "tool_name": "Web Framework Benchmarking",
    "id": "345b911e-015c-4d8c-bbc6-198338f199bf",
    "description": "A variation of the \"query\" workload that exercises the ORM's persistence of objects and the database driver's performance at running UPDATE statements or similar. ",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "nodejs",
      "test": "update"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "FileBench Abstract Workload",
    "tool_name": "FileBench",
    "id": "b214511a-a6d9-420b-8ed9-2f74cd79ffda",
    "description": "Abstract workload for FileBench",
    "categories": [
      "Disk Intensive"
    ],
    "abstract": true,
    "install_ubuntu": "sudo apt-get update; sudo DEBIAN_FRONTEND=noninteractive apt-get -y install bison make libtool automake autoconf flex; %(install_all)s",
    "install_debian": "sudo apt-get update; sudo DEBIAN_FRONTEND=noninteractive apt-get -y install bison make libtool automake autoconf flex wget; %(install_all)s",
    "install_centos": "sudo yum -y install bison make libtool automake autoconf flex; %(install_all)s",
    "install_all": "wget -q https://github.com/filebench/filebench/archive/1.4.9.1.tar.gz; tar xzvf 1.4.9.1.tar.gz; mv filebench-1.4.9.1 filebench; cd filebench; libtoolize; aclocal; autoheader; automake --add-missing; autoconf; ./configure; make",
    "execute": "sudo su -c 'echo \"0\" > /proc/sys/kernel/randomize_va_space'; cp filebench/workloads/%(workload_file)s workload.f; for i in $(echo \"%(custom_params)s\"); do sed \"s/set \\$$(echo \"$i\" | grep -o '^.*=' -).*/set \\$$i/\" -i workload.f; done; echo 'run %(run_param)s' >> workload.f; sudo filebench/filebench -f workload.f",
    "workload_parameters": {},
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.FileBenchResultParser"
  },
  {
    "workload_name": "DaCapo Abstract Workload",
    "tool_name": "DaCapo",
    "id": "266310b7-4f8a-4942-8820-8830ffaa0334",
    "description": "Abstract workload for DaCapo",
    "categories": [
      "CPU Intensive",
      "Memory Intensive",
      "Java"
    ],
    "abstract": true,
    "install_ubuntu_20": "sudo apt-get update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -y install openjdk-14-jre-headless wget\n%(install_all)s",
    "install_ubuntu_18": "sudo apt-get update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -y install openjdk-11-jre-headless wget\n%(install_all)s",
    "install_ubuntu_16": "sudo apt-get update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -y install openjdk-9-jre-headless wget\n%(install_all)s",
    "install_ubuntu_14": "sudo apt-get update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -y install openjdk-7-jre-headless wget\n%(install_all)s",
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install openjdk-11-jre-headless wget\n%(install_all)s",
    "install_centos": "sudo yum -y install java-1.7.0-openjdk wget\n%(install_all)s",
    "install_all": "test ! -f /tmp/dacapo.jar && wget https://sourceforge.net/projects/dacapobench/files/9.12-bach-MR1/dacapo-9.12-MR1-bach-java6.jar -O /tmp/dacapo.jar || echo \"File already exist. Do not download\"",
    "execute": "java -jar /tmp/dacapo.jar -C %(workload_id)s",
    "workload_parameters": {
      "workload_id": "<override me>"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.DaCapoResultParser"
  },
  {
    "workload_name": "WFB Abstract Workload",
    "tool_name": "Web Framework Benchmarking",
    "description": "Abstract workload for Web Framework Benchmarking",
    "categories": [
      "Memory Intensive",
      "Network Intensive"
    ],
    "abstract": true,
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install docker.io unzip\n# download FrameworkBenchmarks from master\ntest ! -f /tmp/frameworkBenchmarks.zip && wget https://codeload.github.com/TechEmpower/FrameworkBenchmarks/zip/master -O /tmp/frameworkBenchmarks.zip || echo \"File already exist. Do not download\"\nunzip /tmp/frameworkBenchmarks.zip\nmv FrameworkBenchmarks-master tool\n# remove the interactive option from the docker invocation, otherwise it will fail\n# when launched from the Benchmarking Suite\nsed -i 's/exec\\ docker\\(.*\\)-it/exec\\ docker\\1/' tool/tfb",
    "install": "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -\nsudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install docker-ce unzip\n# download FrameworkBenchmarks from master\ntest ! -f /tmp/frameworkBenchmarks.zip && wget https://codeload.github.com/TechEmpower/FrameworkBenchmarks/zip/master -O /tmp/frameworkBenchmarks.zip || echo \"File already exist. Do not download\"\nunzip /tmp/frameworkBenchmarks.zip\nmv FrameworkBenchmarks-master tool\n# remove the interactive option from the docker invocation, otherwise it will fail\n# when launched from the Benchmarking Suite\nsed -i 's/exec\\ docker\\(.*\\)-it/exec\\ docker\\1/' tool/tfb",
    "execute": "# if there is < 2GB available, try to prune Docker objects to get more space\ntest `df -k --output=avail / |tail -1` -lt 2000000 && sudo docker system prune -af\ncd tool\nsudo ./tfb --test %(framework)s --type %(test)s | tee output && echo -e \"@@@ results.json content @@@\\n\" && cat `find results -name results.json` && echo -e \"\\n@@@@@@\"\n# kill all containers (if test fails, some containers might remain running)\ntest `sudo docker ps -q | wc -l` -ne \"0\" && sudo docker kill $(sudo docker ps -q)\n# since the tfb command does not exist with a status !=0 in case of failures, we store the output and search for \"NO RESULTS\" string\n! grep \"NO RESULTS\" output",
    "workload_parameters": {},
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "flask-query",
    "tool_name": "Web Framework Benchmarking",
    "id": "a4529244-4790-4a93-a22f-c6c3bdb55b88",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "query",
      "framework": "flask"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "flask-plaintext",
    "tool_name": "Web Framework Benchmarking",
    "id": "2ef2961c-7afa-4a1c-91bc-7f2a87abce35",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "plaintext",
      "framework": "flask"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "flask-json",
    "tool_name": "Web Framework Benchmarking",
    "id": "c653adcc-ddc6-4738-adb8-6b36b2958d8b",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "json",
      "framework": "flask"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "flask-fortune",
    "tool_name": "Web Framework Benchmarking",
    "id": "fb4959b3-995a-4cc0-a8a3-148f63857e36",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "flask",
      "test": "fortune"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "flask-db",
    "tool_name": "Web Framework Benchmarking",
    "id": "81d98886-d46d-4f58-9227-ec2aee262c01",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "db",
      "framework": "flask"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "flask-update",
    "tool_name": "Web Framework Benchmarking",
    "id": "04fcd54f-a4cd-45ca-a754-3fa781f6b150",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "flask",
      "test": "update"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "fasthttp-query",
    "tool_name": "Web Framework Benchmarking",
    "id": "d2405609-5a32-41db-bc38-35b44d9ea394",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "fasthttp",
      "test": "query"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "fasthttp-plaintext",
    "tool_name": "Web Framework Benchmarking",
    "id": "d48ef84f-0f05-4b3b-9356-de8e9e26e5e4",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "plaintext",
      "framework": "fasthttp"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "fasthttp-json",
    "tool_name": "Web Framework Benchmarking",
    "id": "d8e92f32-8bc9-4da9-8745-cbc75ff4af26",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "fasthttp",
      "test": "json"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "fasthttp-fortune",
    "tool_name": "Web Framework Benchmarking",
    "id": "5914ea66-89e1-4672-b64c-3a6c92057519",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "fortune",
      "framework": "fasthttp"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "fasthttp-db",
    "tool_name": "Web Framework Benchmarking",
    "id": "b0f74ca8-4680-442f-981f-a95f4103e115",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "framework": "fasthttp",
      "test": "db"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "fasthttp-update",
    "tool_name": "Web Framework Benchmarking",
    "id": "631b6fc3-46bb-42f4-a9bc-02e5cb653d26",
    "description": "no description",
    "categories": [],
    "parent_workload_id": "8b8d68fd-e626-4b5d-8ca6-fc75e7a93213",
    "workload_parameters": {
      "test": "update",
      "framework": "fasthttp"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.WebFrameworksBenchmarksParser"
  },
  {
    "workload_name": "MySysbench",
    "tool_name": "SysBench",
    "categories": [],
    "parent_workload_id": "2e358596-b13b-4eb1-8865-dc1ef9c7a44b",
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install curl\ncurl -s https://packagecloud.io/install/repositories/akopytov/sysbench/script.deb.sh | sudo bash\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install sysbench\nsysbench %(test)s prepare %(options)s || echo \"not implemented\"",
    "install_ubuntu": "%(install_debian)s",
    "workload_parameters": {},
    "grants": {
      "owner": "usr:gabriele"
    },
    "id": "9d18a0dc-01b3-413b-8241-f5e28a22e9fe",
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.SysbenchResultParser"
  },
  {
    "workload_name": "YCSB MYSQL Abstract Workload",
    "tool_name": "YCSB MySQL",
    "id": "ff79cf44-e263-47be-be5b-8e1e8fc6479c",
    "categories": [
      "database",
      "mysql"
    ],
    "abstract": true,
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install mariadb-server openjdk-11-jre-headless python libmariadb-java wget\nsudo cp /usr/share/java/mariadb-java-client.jar /tmp/driver.jar\necho \"\nDROP database if exists benchmark;\nDROP user if exists 'testuser'@localhost;\ncreate database benchmark;\nCREATE user 'testuser'@localhost IDENTIFIED BY 'password';\nGRANT ALL ON benchmark.* TO 'testuser' IDENTIFIED BY 'password';\n\" | sudo mysql\n%(install_all)s",
    "install_all": "test ! -f /tmp/ycsb-jdbc.tar.gz && wget https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-jdbc-binding-0.17.0.tar.gz -O /tmp/ycsb-jdbc.tar.gz || echo \"File already exist. Do not download\"\ntar xzvf /tmp/ycsb-jdbc.tar.gz\nmv ycsb-jdbc-binding-0.17.0 ycsb\necho \"\nuse benchmark;\ndrop table if exists usertable;\nCREATE TABLE usertable (\n\tYCSB_KEY VARCHAR(255) PRIMARY KEY,\n\tFIELD0 TEXT, FIELD1 TEXT,\n\tFIELD2 TEXT, FIELD3 TEXT,\n\tFIELD4 TEXT, FIELD5 TEXT,\n\tFIELD6 TEXT, FIELD7 TEXT,\n\tFIELD8 TEXT, FIELD9 TEXT\n);\n\" | mysql -utestuser -ppassword\ncd ycsb\necho \"%(workload)s\" > workload.txt\nbin/ycsb load jdbc -P workload.txt -p db.url=jdbc:mysql://127.0.0.1:3306/benchmark -p db.user=testuser -p db.passwd=password -p db.batchsize=10000 -p jdbc.batchupdateapi=true -p jdbc.autocommit=false -p db.driver=org.mariadb.jdbc.Driver -cp /tmp/driver.jar | tee output\n# since the ycsb commands do not exit with an exit status !=0 if the load or run contain errors, we search for the FAILED word in the output\n! grep FAILED output",
    "execute": "cd ycsb\nbin/ycsb run jdbc -P workload.txt -p db.url=jdbc:mysql://127.0.0.1:3306/benchmark -p db.user=testuser -p db.passwd=password -p db.driver=org.mariadb.jdbc.Driver -cp /tmp/driver.jar | tee output\n# since the ycsb commands do not exit with an exit status !=0 if the load or run contain errors, we search for the FAILED word in the output\n! grep FAILED output",
    "workload_parameters": {
      "default_ops_count": "100000"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
  },
  {
    "workload_name": "YCSB MySql Workload A",
    "tool_name": "YCSB MySQL",
    "id": "1113db91-257c-4455-9afe-201a18221c59",
    "categories": [],
    "parent_workload_id": "ff79cf44-e263-47be-be5b-8e1e8fc6479c",
    "workload_parameters": {
      "workload": "recordcount=%(default_ops_count)s\noperationcount=%(default_ops_count)s\nworkload=site.ycsb.workloads.CoreWorkload\nreadallfields=true\nreadproportion=0.5\nupdateproportion=0.5\nscanproportion=0\ninsertproportion=0\nrequestdistribution=zipfian"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
  },
  {
    "workload_name": "YCSB MongoDB Abstract Workload",
    "tool_name": "YCSB MongoDB",
    "id": "da72cd23-dc6e-4ac3-bcd4-036f51c97918",
    "categories": [
      "database",
      "mysql"
    ],
    "abstract": true,
    "install_debian": "sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install openjdk-11-jre python wget\nwget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -\necho \"deb http://repo.mongodb.org/apt/debian buster/mongodb-org/4.4 main\" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq update\nsudo DEBIAN_FRONTEND=noninteractive apt-get -yq install mongodb-org\nsudo mkdir -p /data/db\nsudo systemctl daemon-reload\nsudo systemctl start mongod\n%(install_all)s",
    "install_all": "wget https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-mongodb-binding-0.17.0.tar.gz\ntar xzvf ycsb-mongodb-binding-0.17.0.tar.gz\nmv ycsb-mongodb-binding-0.17.0 ycsb\ncd ycsb\necho \"%(workload)s\" > workload.txt\n# wait to let mongo server come online\nmongo --eval \"db.stats()\" || sleep 60\nmongo ycsb --eval \"db.dropDatabase()\"\nbin/ycsb load mongodb-async -s -P workload.txt | tee output\n# since the ycsb commands do not exit with an exit status !=0 if the load or run contain errors, we search for the FAILED word in the output\n! grep FAILED output",
    "execute": "cd ycsb\nbin/ycsb run mongodb-async -s -P workload.txt\n# since the ycsb commands do not exit with an exit status !=0 if the load or run contain errors, we search for the FAILED word in the output\n! grep FAILED output",
    "workload_parameters": {
      "default_ops_count": "100000"
    },
    "grants": {
      "executor": "org:public",
      "owner": "usr:paolo"
    },
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
  },
  {
    "workload_name": "YCSB MongoDB Workload A",
    "tool_name": "YCSB MongoDB",
    "categories": [],
    "parent_workload_id": "da72cd23-dc6e-4ac3-bcd4-036f51c97918",
    "workload_parameters": {
      "workload": "recordcount=%(default_ops_count)s\noperationcount=%(default_ops_count)s\nworkload=site.ycsb.workloads.CoreWorkload\nreadallfields=true\nreadproportion=0.5\nupdateproportion=0.5\nscanproportion=0\ninsertproportion=0\nrequestdistribution=zipfian"
    },
    "grants": {
      "owner": "usr:paolo"
    },
    "id": "e0829aec-6522-44e4-952c-9d9789af042f",
    "class": "benchsuite.stdlib.benchmark.vm_benchmark.BashCommandBenchmark",
    "parser": "benchsuite.stdlib.benchmark.parsers.YCSBResultParser"
  }
]
