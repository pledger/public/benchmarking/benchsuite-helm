/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var providers =
[
  {
    "name" : "FIVicTpl",
    "description" : "Default description",
    "grants" : {
        "owner" : "usr:gabriele",
        "viewer" : "org:public"
    },
    "driver" : "openstack",
    "auth_version" : "2.0_password",
    "tenant" : "ETICS",
    "auth_url" : "http://cloud.lab.fiware.org:4730/",
    "region" : "Vicenza",
    "access_id" : "",
    "secret_key" : "",
    "service_properties" : {
        "post_create_script" : {
            "debian" : "sudo hostname localhost",
            "centos" : "sudo sed -i 's/requiretty/!requiretty/' /etc/sudoers"
        }
    },
    "id" : "26940e91-360f-42b4-be49-1147e034cc8e",
    "class" : "benchsuite.stdlib.provider.libcloud.LibcloudComputeProvider",
    "network" : "node-int-net-01"
  }
]