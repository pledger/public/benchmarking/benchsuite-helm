/* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

load('/docker-entrypoint-initdb.d/workloads.data');
load('/docker-entrypoint-initdb.d/providers.data');

db = new Mongo().getDB("benchsuite");
db.createCollection('workloads', { capped: false });
db.workloads.insert(workloads);

db.createCollection('providers', { capped: false });
db.providers.insert(providers);

db.createCollection('executions', { capped: false });
db.executions.createIndex({ "starttime": 1 })