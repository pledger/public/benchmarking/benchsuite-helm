{{- /* Benchmarking Suite
 * Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */ -}}

{{- if .Values.scheduler.enabled -}}
{{- if and .Values.influxdb.enabled .Values.storage.influx -}}
[Results]
class=benchsuite.backend.influxdb_for_pledger.InfluxDBStorageConnectorForPledger
hostname = {{ include "cybco.call-in-virtual-context" (list "influxdb.fullname" . "influxdb" "") }}
port = 8086
username = bswrite
password = benchpwd
db_name = benchsuite
enabled=True
{{- end }}
{{ if and .Values.mongodb.enabled .Values.storage.mongodb }}
[Logs]
class = benchsuite.backend.mongodb.MongoDBStorageConnector
connection_string = mongodb://{{ include "cybco.call-in-virtual-context" (list "mongodb.fullname" . "mongodb" "") }}:27017
db_name = benchsuite
collection_name = results
store_logs=True
store_metrics=True
enabled=True
{{- end }}
{{ if .Values.storage.log }}
[StdOut]
class=benchsuite.backend.stdout.StdOutStorageConnector
enabled=True
{{- end }}
{{- end }}
