#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

apiVersion: 1
datasources:
  - name: BenchsuiteMetrics
    version: 1
    type: influxdb
    access: proxy
    orgId: 1
    url: {{ include "cybco.call-in-virtual-context" (list "cybco.service.url" . "" "influx-proxy") }}
    user: bsread
    database: benchsuite
    isDefault: true
    jsonData:
      oauthPassThru: true
    secureJsonData:
      password: benchpwd
    editable: false