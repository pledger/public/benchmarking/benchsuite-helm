# Helm chart to deploy the Benchsuite

## Instructions
- create Keycloak CRDs (if they do not exist)
  
  ```bash
  git clone https://github.com/kiwigrid/keycloak-controller.git
  cd keycloak-controller
  kubectl apply -f src/main/k8s/
  ```
  
- create imagePull secret
  
  ```bash
  k create secret -n benchmarking-dev docker-registry gitlab-registry-pull-credentials   --docker-server=https://registry.res.eng.it/   --docker-username=gitlab+deploy-token-10  --docker-password=xxxx --docker-email=gabriele.giammatteo@eng.it
  ```
  
- deploy the chart
  
  ```bash
  helm upgrade --install bes1 . --debug --namespace benchmarking-dev --values eng-values.yaml
  ```

- users must be created in keycloak
  
  - get admin password
  
  ```bash
  k get secret bes-keycloak-credentials -o jsonpath=&#39;{.data.adminPassword}&#39; | base64 --decode && echo &quot;
  ```
  
  - access to http://192.168.111.52:30100/auth and create users


- add "view-users" role from the realm-managemnet to the rest client's "Service Account Roles" tab. If the "Service Account Roles" tab is not shown, make sure that the "Service Accounts Enabled" option is enabled in the client "Settings" tab

- to allow a user to change Grafana Dashboards, it needs to have the role "admin" (from the Grafana client). It can be assinged in Keycloak GUI from User -> Role Mapping -> Clients Roles...

- to allow the client used by the pledger-sync to read metrics in influxdb, add the scope "influx-admin" in the "Assigned Default Client Scopes" list (it should be already in the "Avaialble" scopes) 

- Access Benchsuite Admin Web Portal at http://192.168.111.52:30300/


If Kyecloak is deployed, the secrets for credentials will be bes-keycloak-credentials and bes-keycloak-postgres-credentials. They are static names (they does not contain the release name) because these names must be provided in the values file (see values.yaml) and they do not go trhough the tpl function.

Grafana and Influx have a stable secret without release name "bes-grafana-admin-credentials

To create init scritps from mongodb exports: 
```
mongoexport --db=benchsuite --collection=workloads --jsonArray --pretty | ./jq '[.[] | del(._id)]'
```

## Troubleshooting

### Error: Unkown Issuer or Invalid Token
If you have similar errors in the keycloak log
```
13:21:31,295 WARN  [org.keycloak.events] (default task-11) type=USER_INFO_REQUEST_ERROR, realmId=c4b44d99-ad06-41ec-8051-3168cf8a59af, clientId=null, userId=null, ipAddress=10.224.77.171, error=invalid_token, auth_method=validate_access_token
```

and errors like

```
KeyError: 'sub'
```

or

```
Unkown Issue
```

in the api, make sure the frontend URL of the Realm matches the ISSUER_URL in the api-deployment

### INVALID_SCOPE or "User does not exists"

If in the influxdb-proxy logs there is INVALID_SCOPE in the queries, it is because the influx-proxy cannot retrieve the user's scopes from the rest.

If the rest replies to `curl 10.111.197.201/api/v3/users/gabriele/scopes` with
```json
{"message": "User 'gabriele' doesn't exist"}
```
it could be that the rest's keycloak client do not have the "view-users" permission (see installation guide)

# Documentation
User and technical documentation is available at http://benchmarking-suite.readthedocs.io/.

# Support
For bugs, enhancements or support go to https://gitlab.res.eng.it/benchsuite

# Legal
The Benchmarking Suite is released under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.

Copyright © 2014-2022 Engineering Ingegneria Informatica S.p.A. All rights reserved.

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received partial funding by the European Commission under grant agreements No. FP7-317859, No. H2020-732258 and No. H2020-871536 and by  EIT Digital within the EasyCloud innovation activity. |
|---|---|